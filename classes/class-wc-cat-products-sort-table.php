<?php

if(!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class WC_Cat_Products_Sort_Table extends WP_List_Table {

    function __construct(array $products) {
        $this->products = $products;
        parent::__construct(array(
             'singular'=> 'Product',
             'plural' => 'Products',
             'ajax'  => true
        ));
    }

    /**
    * Define the columns that are going to be used in the table
    * @return array $columns, the array of columns to use with the table
    */
    function get_columns() {
        return array(
            'cb'        => '<input type="checkbox" />',
            'post_title' => __('Visible Products') . ' (' . count($this->products) . ')',
        );
    }

    /**
     * callback for 'cb' checkbox column
     */
    function column_cb($item) {
         return sprintf(
             '<input type="checkbox" name="post[]" value="%s" />', $item['ID']
         );    
     }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
        $sortable = array(
        );
        return $sortable;
    }

    /**
     * Prepare the table with different parameters, pagination, columns and table elements
     */
    function prepare_items() {

        foreach($this->products as $product) {
            $this->items[] = (array) $product;
        }
        $columns = $this->get_columns();
        $this->_column_headers = array($columns, array(), $this->get_sortable_columns());
    }

    /** 
     * default callback for columns
     */
    function column_default($item, $column_name){
        return $item[$column_name];
    }

	function display() {
		extract( $this->_args ); 
	?>
	<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>" cellspacing="0">
	<thead>
	<tr>
		<?php $this->print_column_headers(); ?>
	</tr>
	</thead>

	<tfoot>
	<tr>
		<?php $this->print_column_headers( false ); ?>
	</tr>
	</tfoot>

	<tbody id="the-list"<?php if ( $singular ) echo " data-wp-lists='list:$singular'"; ?>>
		<?php $this->display_rows_or_placeholder(); ?>
	</tbody>
	</table>
	<?php
	}
}

