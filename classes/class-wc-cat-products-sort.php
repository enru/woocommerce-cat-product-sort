<?php

require 'class-wc-cat-products-sort-table.php';

class WC_Cat_Products_Sort {

    public function __construct() {
        $this->plugin_dir_url = plugin_dir_url(dirname(__FILE__));

        add_action('product_cat_edit_form', array($this, 'cat_products_sort_form'), 10, 1 );
        add_action('product_tag_edit_form', array($this, 'cat_products_sort_form'), 10, 1 );

        add_filter('posts_fields', array($this, 'posts_fields'), 99, 2);
        add_filter('posts_join', array($this, 'posts_join'), 99, 2);
        add_filter('posts_orderby', array($this, 'posts_orderby'), 99, 2);

        add_action('admin_enqueue_scripts', array($this, 'sortable_product_scripts' ));
        add_action('wp_ajax_cat_products_sort_ordering', array($this, 'cat_products_sort_ordering' ));
    }

    /**
    * displays a product sorting form in product category edit pages
    **/
    function cat_products_sort_form($term) {

        $args = array(
            'nopaging' => true,
            'post_type' => 'product',
			'post_status' => 'publish',
            'meta_query' => array (
                array (
                    'key' => '_visibility',
                    'value' => array('visible', 'catalog'),
                    'compare' => 'IN',
                ),
            ),
			'tax_query' => array(
				array(
					'taxonomy' => $term->taxonomy,
					'field' => 'slug',
					'terms' => $term->slug,
				)
			)
		);

        $products = new WP_Query($args);
	
        echo '<h2>Product Ordering</h2>';

        $products_table = new WC_Cat_Products_Sort_Table($products->posts);
        $products_table->prepare_items();
        $products_table->display();
    }

    /**
    * adds `sortval` field to posts db COLUMNS 
    **/
    function posts_fields($fields, $query) {
        if(isset($query->query_vars['taxonomy'])) {
		if($query->query_vars['taxonomy'] == 'product_cat' || $query->query_vars['taxonomy'] == 'product_tag') {
		    $my_fields = ', cast(wpmeta.meta_value as UNSIGNED) as sortval'; 
		    $fields .= (preg_match('/^(\s+)?,/',$my_fields)) ? $my_fields: ", $my_fields";
		}
	}
        return $fields;
    }

    /**
    * adds `sortval` field to posts db JOIN
    **/
    function posts_join($join, $query) {
        global $wpdb;
        if(isset($query->query_vars['taxonomy'])) {
		if(($query->query_vars['taxonomy'] == 'product_cat' || $query->query_vars['taxonomy'] == 'product_tag') 
		    && isset($query->query_vars['term'])) {
		    $term = get_term_by('slug', $query->query_vars['term'], $query->query_vars['taxonomy']);
		    $sort_key = 'product_order_' . $term->term_id;
		    $join .= " LEFT JOIN {$wpdb->postmeta} wpmeta ON ("
			."{$wpdb->posts}.ID = wpmeta.post_id "
			." AND wpmeta.meta_key = '$sort_key')";
		}
	}
        return $join;
    }

    /**
    * adds `sortval` field to posts db ORDER
    **/
    function posts_orderby($orderby, $query) {
        if(isset($query->query_vars['taxonomy'])) {
		if($query->query_vars['taxonomy'] == 'product_cat' || $query->query_vars['taxonomy'] == 'product_tag') {
		    global $wpdb;
		    $orderby = "sortval ASC, {$wpdb->posts}.post_title ASC";
		}
	}
	return $orderby;
    }

    /**
    * queues our product ordering JS file in the admin panel 
    **/
    function sortable_product_scripts() {
        
        global $woocommerce;
        $screen = get_current_screen();

        wp_enqueue_script( 'jquery-ui-sortable' );

        // Product sorting - only when sorting by menu order on the product category page
        if ( current_user_can('edit_others_pages') 
            && ($screen->id == 'edit-product_cat' 
            || $screen->id == 'edit-product_tag') 
            //&& isset( $wp_query->query['orderby'] ) 
            //&& $wp_query->query['orderby'] == 'menu_order title' 
            ) {

            wp_enqueue_script(
                'cat_products_sort_ordering', 
                $this->plugin_dir_url . '/js/admin/product-ordering.js', 
                array('jquery-ui-sortable'), '1.0', true );

        }
    }

    /**
    * queues our product ordering JS file in the admin panel 
    **/
    function cat_products_sort_ordering() {
        global $wpdb;

        // check permissions again and make sure we have what we need
        if ( ! current_user_can('edit_others_pages') || 
            empty( $_POST['id'] ) || 
            ( ! isset( $_POST['previd'] ) && ! isset( $_POST['nextid'] ) && ! isset($_POST['tagid']) ) )
            die(-1);

        // real post?
        if ( ! $post = get_post( $_POST['id'] ) )
            die(-1);

        $taxonomy = isset( $_POST['tax'] ) ? $_POST['tax'] : '';
        $tagid = isset( $_POST['tagid'] ) ? $_POST['tagid'] : '';
        $previd = isset( $_POST['previd'] ) ? $_POST['previd'] : false;
        $nextid = isset( $_POST['nextid'] ) ? $_POST['nextid'] : false;
        $new_pos = array(); // store new positions for ajax

        $sort_key = 'product_order_' . $tagid;

        $db_query = $wpdb->prepare("
            SELECT ID, {$wpdb->postmeta}.meta_value as menu_order FROM {$wpdb->posts} 
            LEFT JOIN {$wpdb->postmeta} ON (
                {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id 
                 AND {$wpdb->postmeta}.meta_key = %s
            )
            JOIN {$wpdb->term_taxonomy} ON (
                ({$wpdb->term_taxonomy}.term_id = %d OR {$wpdb->term_taxonomy}.parent = %d)
                AND {$wpdb->term_taxonomy}.taxonomy = %s
            )
            JOIN {$wpdb->term_relationships} ON (
                {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->term_taxonomy}.term_taxonomy_id 
                AND {$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id
            )
            WHERE 	{$wpdb->posts}.post_type 	= 'product'
            AND 	{$wpdb->posts}.post_status 	= 'publish'
            AND 	{$wpdb->posts}.ID			NOT IN ( {$post->ID} )
            ORDER BY CAST({$wpdb->postmeta}.meta_value as UNSIGNED) ASC, {$wpdb->posts}.post_title ASC
        ",
        $sort_key,
        $tagid,
        $tagid,
        $taxonomy);

        $siblings = $wpdb->get_results($db_query);

        $menu_order = 0;

        foreach( $siblings as $sibling ) {

            // if this is the post that comes after our repositioned post, set our repositioned post position and increment menu order
            if ( $nextid == $sibling->ID ) {
                update_post_meta($post->ID, $sort_key, $menu_order);
                $new_pos[ $post->ID ] = $menu_order;
                $menu_order++;
            }

            // if repositioned post has been set, and new items are already in the right order, we can stop
            if ( isset( $new_pos[ $post->ID ] ) && $sibling->menu_order >= $menu_order )
                break;

            // set the menu order of the current sibling and increment the menu order
            update_post_meta($sibling->ID, $sort_key, $menu_order);
            $new_pos[ $sibling->ID ] = $menu_order;
            $menu_order++;

            if ( ! $nextid && $previd == $sibling->ID ) {
                update_post_meta($post->ID, $sort_key, $menu_order);
                $new_pos[$post->ID] = $menu_order;
                $menu_order++;
            }

        }

        die( json_encode( $new_pos ) );
    }

}


