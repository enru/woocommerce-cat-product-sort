<?php
/*
Plugin Name: WooCommerce Category Product Sort
Plugin URI: http://inigo.net/woocommerce-cat-product-sort
Description: Extends WooCommerce and allows sorting of products within categories
Version: 0.1
Author: Neill Russell (@enru)  
Author URI: http://enru.co.uk/

Copyright: © 2012 Inigo Media Ltd.
License: GNU General Public License v2.0
License URI: http://www.gnu.org/old-licenses/gpl-2.0.html
*/


/**
 * Check if WooCommerce is active
 **/
if(in_array('woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' )))) {

    function woocommerce_cat_products_sort_init() {

        if(class_exists('WC_Cat_Products_Sort')) return;

        /**
        * Localisation
        */
        load_plugin_textdomain(
            'wc-cat-products-sort', 
            false, 
            dirname(plugin_basename( __FILE__ )) . '/languages');

        require 'classes/class-wc-cat-products-sort.php';
        $sort = new WC_Cat_Products_Sort();

    }
    add_action('plugins_loaded', 'woocommerce_cat_products_sort_init', 0);

}// end if woocommerce
